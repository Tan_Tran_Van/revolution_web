// show and hide menubar
const getElement = (selector) => {
    return document.querySelector(selector);
}

// show mune when scrolltop >10px 
window.onscroll = () => { scrollHeader() };

const scrollHeader = () => {
    if (document.documentElement.scrollTop > 10) {
        getElement(".headershow").classList = "headershow active";
    } else {
        getElement(".headershow").classList = "headershow";
    }
}
// show and hide menubar
getElement(".header__menuWnavBar").addEventListener("click", () => {
    const headerMenu = getElement(".header__menuWhite");
    if (headerMenu.classList == "header__menuWhite show") {
        headerMenu.classList = "header__menuWhite hide";
    }
    else {
        headerMenu.classList = "header__menuWhite show";
    }
    const headerMenu1 = getElement(".header__menuBack");
    if (headerMenu1.classList == "header__menuBack hide closemenubar") {
        headerMenu1.classList = "header__menuBack show openmenubar";
    }
    else {
        headerMenu1.classList = "header__menuBack hide closemenubar";
    }
})
getElement(".header__menuBnavavBar").addEventListener("click", () => {
    const headerMenu = getElement(".header__menuBack");
    if (headerMenu.classList == "header__menuBack show openmenubar") {
        headerMenu.classList = "header__menuBack hide closemenubar";
    }
    else {
        headerMenu.classList = "header__menuBack show openmenubar";
    }
    const headerMenu1 = getElement(".header__menuWhite");
    if (headerMenu1.classList == "header__menuWhite hide") {
        headerMenu1.classList = "header__menuWhite show";
    }
    else {
        headerMenu1.classList = "header__menuWhite hide";
    }
})

